class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        return self.products.append(product)

    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    def get_total_quantity_of_products(self):
        return sum(product.quantity for product in self.products)

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.quantity * self.unit_price


if __name__ == '__main__':
    test_product = Product('Shoes', 100.0)
    shoes = Product('Shoes', 30.00, 3.0)
    tshirt = Product('T-Shirt', 50.00, 2.0)
    bag = Product('Bag', 10.00)
    print(test_product.get_price())
    print(shoes.get_price())
    print(tshirt.get_price())

    # Order with no products
    order_1 = Order('adrian@example.com')
    print(order_1.get_total_price())

    # Order with one product
    order_2 = Order('adrian@example.com')
    order_2.add_product(shoes)
    print(order_2.get_total_price())

    # Order with three products
    order_3 = Order('adrian@example.com')
    order_3.add_product(shoes)
    order_3.add_product(tshirt)
    order_3.add_product(bag)
    print(order_3.get_total_price())
    print(order_3.get_total_quantity_of_products())

    # Test order purchase
    order_4 = Order('adrian@example.com')
    order_4.add_product(shoes)
    order_4.purchase()
    print(order_4.purchased)